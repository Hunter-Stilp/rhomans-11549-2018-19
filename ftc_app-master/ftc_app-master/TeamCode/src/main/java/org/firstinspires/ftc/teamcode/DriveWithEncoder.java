//program that drives bot forward a set distance, stops then
// backs up to the starting point using encoders to measure the distance.
// This example assumes there is one encoder, attached to the left motor.

        package org.firstinspires.ftc.teamcode;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.DcMotorController;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import java.util.List;
import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer.CameraDirection;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import com.qualcomm.robotcore.util.ElapsedTime;


@Autonomous(name="DriveEncoder2", group="Pushbot")
//@Disabled
public class DriveWithEncoder extends LinearOpMode
{


    public static int motorPPR = 28;
    public static double wheelDiameter = 3.25;
    public static double sideToSideWheelDistanceMM = 395;
    public static int gearRatio = 20;
    public static int pingsOfLift = -24800;
    public static int millisecondsHookOpen = 1500;
    public static int millisecondsHookClose = 500;
    private ElapsedTime     runtime = new ElapsedTime();


    private static  String TFOD_MODEL_ASSET;
    private static String LABEL_GOLD_MINERAL;
    private static String LABEL_SILVER_MINERAL;
    private static int mineralPosition;


    private static String VUFORIA_KEY;

    private VuforiaLocalizer vuforia;

    private TFObjectDetector tfod;


    private BNO055IMU gyro;

    public  static int hookTimeLeft = 1100;

    DcMotor liftMotor;

    DcMotor leftMotor;
    DcMotor rightMotor;
    DcMotor leftMotor2;
    DcMotor rightMotor2;
    Servo hook;
    Servo marker;

    @Override
    public void runOpMode() {

        gyro = hardwareMap.get(BNO055IMU.class, "imu" );

        initGyro();

        initHook();

        initMotorsForward();

        initLiftForwards();

        initOD();

        telemetry.addData("Mode", "waiting");
        telemetry.update();

        // wait for start button.

        waitForStart();

        telemetry.addData("Mode", "running");
        telemetry.update();

        // set left motor to run for 5000 encoder counts.

      //  leftMotor.setTargetPosition(5000);

        // set both motors to 25% power. Movement will start.
      //  driveForward(-5000, -0.2);

        // wait while opmode is active and left motor is busy running to position.
        int leftTicks = leftMotor.getCurrentPosition();
        int rightTicks = rightMotor.getCurrentPosition();
        int leftTicks2 = leftMotor2.getCurrentPosition();
        int rightTicks2 = rightMotor2.getCurrentPosition();
        runSudo();

       // driveForward(610, -1);


  //      leftMotor.setTargetPosition(-5000);
  //      leftMotor.setPower(-0.2);


        /*
        driveTurnClockwise2(584, 180, 0.39);

        while (opModeIsActive() && leftMotor.isBusy()) {
            printData();
            idle();

        }
        stopAndResetEncoders();
*/


        stopMotors();





    }



    public void liftExtend(){
        initMotorsForward();
            liftMotor.setTargetPosition(pingsOfLift);
            liftMotor.setPower(1);
    }

    public void liftContract(){
        initLiftBackwards();
        liftMotor.setTargetPosition(pingsOfLift);
        liftMotor.setPower(1);
    }

    public void hookOpen(){
        hook.setPosition(1);
    }

    public void hookClose(){
        hook.setPosition(0);
    }

    public void markerDrop(){
        marker.setPosition(1);
    }

    public void hookStop(){
        hook.setPosition(0.5);
    }

    public void markerStop(){
        marker.setPosition(0.5);
    }

    public void driveTurnClockwise2(double turnRadius,double degrees, double power){

        if (degrees<0){
            driveTurnClockwiseNegitive(turnRadius,degrees, power);
        }
        else {

            double minorRadius = turnRadius - (sideToSideWheelDistanceMM / 2.0);
            double majorRadius = turnRadius + (sideToSideWheelDistanceMM / 2.0);

            double minorCircum = 2.0 * 3.1415926 * minorRadius;
            double majorCircum = 2.0 * 3.1415926 * majorRadius;


            double minorDistance = minorCircum * (degrees / 360);
            double majorDistance = majorCircum * (degrees / 360);

            printString("minorCircum = " + minorDistance + "majorCircum = " + majorDistance);

            if (0 < majorDistance) {
                double minorSpeed = (minorDistance / majorDistance) * power;
                double majorSpeed = power;

                leftMotor.setTargetPosition(mmToPings(majorDistance));
                leftMotor.setPower(majorSpeed);

                rightMotor.setTargetPosition(mmToPings(minorDistance));
                rightMotor.setPower(minorSpeed);


                leftMotor2.setTargetPosition(mmToPings(majorDistance));
                leftMotor2.setPower(majorSpeed);

                rightMotor2.setTargetPosition(mmToPings(minorDistance));
                rightMotor2.setPower(minorSpeed);
            }

            if (0 > majorDistance) {
                double majorSpeed = -(majorDistance / minorDistance) * power;
                double minorSpeed = -power;

                leftMotor.setTargetPosition(-mmToPings(majorDistance));
                leftMotor.setPower(majorSpeed);

                rightMotor.setTargetPosition(-mmToPings(minorDistance));
                rightMotor.setPower(minorSpeed);


                leftMotor2.setTargetPosition(-mmToPings(majorDistance));
                leftMotor2.setPower(majorSpeed);

                rightMotor2.setTargetPosition(-mmToPings(minorDistance));
                rightMotor2.setPower(minorSpeed);

                printString("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            }


        }

    }


    public void driveTurnClockwiseNegitive(double turnRadius,double degrees, double power){
initMotorsBackward();
       // power = power;
        turnRadius = -turnRadius;
        double minorRadius = turnRadius-(sideToSideWheelDistanceMM/2.0);
        double majorRadius = turnRadius+(sideToSideWheelDistanceMM/2.0);

        double minorCircum = 2.0*3.1415926*minorRadius;
        double majorCircum = 2.0*3.1415926*majorRadius;


        double minorDistance = minorCircum * (degrees/360);
        double majorDistance = majorCircum * (degrees/360);

        printString("minorCircum = " + minorDistance + "majorCircum = " + majorDistance);

        if(0<majorDistance){
            double minorSpeed = -(minorDistance/majorDistance)*power;
            double majorSpeed = -power;

            leftMotor.setTargetPosition (mmToPings(majorDistance));
            leftMotor.setPower(majorSpeed);

            rightMotor.setTargetPosition(mmToPings(minorDistance));
            rightMotor.setPower(minorSpeed);


           // leftMotor2.setTargetPosition(mmToPings(majorDistance));
          //  leftMotor2.setPower(majorSpeed);

            rightMotor2.setTargetPosition(mmToPings(minorDistance));
            rightMotor2.setPower(minorSpeed);
        }

        if(0>majorDistance){
            double majorSpeed = (majorDistance/minorDistance)*power*1;
            double minorSpeed = power*1;

            leftMotor.setTargetPosition(-mmToPings(majorDistance));
            leftMotor.setPower(majorSpeed);

            rightMotor.setTargetPosition(-mmToPings(minorDistance));
            rightMotor.setPower(minorSpeed);


            leftMotor2.setTargetPosition(-mmToPings(majorDistance));
            leftMotor2.setPower(majorSpeed);

            rightMotor2.setTargetPosition(-mmToPings(minorDistance));
            rightMotor2.setPower(minorSpeed);

            printString("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        }





    }

    public void driveForward(double distanceMilimeters, double power){


        leftMotor.setTargetPosition(mmToPings(distanceMilimeters));
        leftMotor.setPower(power);

        rightMotor.setTargetPosition(mmToPings(distanceMilimeters));
        rightMotor.setPower(power);

        leftMotor2.setTargetPosition(mmToPings(distanceMilimeters));
        leftMotor2.setPower(power);

        rightMotor2.setTargetPosition(mmToPings(distanceMilimeters));
        rightMotor2.setPower(power);

    }


    public void centerTurnClockwise(double degrees, double power){



        double circumferenceOfTurn = sideToSideWheelDistanceMM * 3.1415;
        double turnDistanceMM = circumferenceOfTurn * (degrees/360.0);


        double leftRailMilimeters = turnDistanceMM;
        double rightRailMilimeters = -turnDistanceMM;

        leftMotor.setTargetPosition(mmToPings(leftRailMilimeters));
        leftMotor.setPower(power);

        rightMotor.setTargetPosition(mmToPings(rightRailMilimeters));
        rightMotor.setPower(power);


        leftMotor2.setTargetPosition(mmToPings(leftRailMilimeters));
        leftMotor2.setPower(power);

        rightMotor2.setTargetPosition(mmToPings(rightRailMilimeters));
        rightMotor2.setPower(power);
    }


    public void stopMotors(){
        leftMotor.setPower(0);
        rightMotor.setPower(0);
        leftMotor2.setPower(0);
        rightMotor2.setPower(0);
    }

    public void stopLift(){
        liftMotor.setPower(0);
    }


    public void initMotorsForward(){

        leftMotor = hardwareMap.dcMotor.get("left_drive");
        rightMotor = hardwareMap.dcMotor.get("right_drive");
        leftMotor2 = hardwareMap.dcMotor.get("left_drive2");
        rightMotor2 = hardwareMap.dcMotor.get("right_drive2");

        rightMotor.setDirection(DcMotor.Direction.REVERSE);
        rightMotor2.setDirection(DcMotor.Direction.REVERSE);
        leftMotor2.setDirection(DcMotor.Direction.REVERSE);
        leftMotor.setDirection(DcMotor.Direction.FORWARD);

        leftMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        leftMotor2.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightMotor2.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        leftMotor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        rightMotor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        leftMotor2.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        rightMotor2.setMode(DcMotor.RunMode.RUN_TO_POSITION);

    }

    public void initMotorsBackward(){
        leftMotor = hardwareMap.dcMotor.get("left_drive");
        rightMotor = hardwareMap.dcMotor.get("right_drive");
        leftMotor2 = hardwareMap.dcMotor.get("left_drive2");
        rightMotor2 = hardwareMap.dcMotor.get("right_drive2");

        rightMotor.setDirection(DcMotor.Direction.FORWARD);
        rightMotor2.setDirection(DcMotor.Direction.FORWARD);
        leftMotor2.setDirection(DcMotor.Direction.FORWARD);
        leftMotor.setDirection(DcMotor.Direction.REVERSE);


        leftMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        leftMotor2.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightMotor2.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        leftMotor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        rightMotor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        leftMotor2.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        rightMotor2.setMode(DcMotor.RunMode.RUN_TO_POSITION);
    }

    public void initLiftForwards(){
        liftMotor = hardwareMap.dcMotor.get("lift_drive");
        liftMotor.setDirection(DcMotor.Direction.REVERSE);
        liftMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        liftMotor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
    }

    public void initLiftBackwards(){
        liftMotor = hardwareMap.dcMotor.get("lift_drive");
        liftMotor.setDirection(DcMotor.Direction.FORWARD);
        liftMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        liftMotor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
    }

    public void initHook(){
        hook = hardwareMap.get(Servo.class, "hook");
    }

    public void initMarker(){
        marker = hardwareMap.get(Servo.class, "marker");
    }




    public void initOD(){
        TFOD_MODEL_ASSET = "RoverRuckus.tflite";
        LABEL_GOLD_MINERAL = "Gold Mineral";
        LABEL_SILVER_MINERAL = "Silver Mineral";
        mineralPosition = 0;
        VUFORIA_KEY = "AVOik8H/////AAABmUCbwUvX/UC8i2vB+PSYYAMSX0YqBFC+33zat2Q/CvVqUeJ0GvFd3I2cq9z0UW2WogegHvtMEN+PYvOQMt4qrZUM2MQkpC0bxdbD91QzX5GhQUHrs0icydu0lO1GLokMc+HSYXXhauQrEteC1XOQ8JXz0FGRd5wbzpfPf58rfSYaqTzD2kAvfNQyK8yMlys+ubR/aiH1nzH99wdFxi1LIyNTJWgMUuKHhHIJvdyazO/Ye74Qd6pVyHov/LEw8A21EL9XyEmFkXutxdq2WCnTM8RLLiLKZubVo7MrRlhhVT5vC0pKUmdersGAJh0hZ3iHdBz2FOHzzbMeG4018DveRA8jxd8gDeEeHDYJXYJ+5k8j";
        initVuforia();









        if (ClassFactory.getInstance().canCreateTFObjectDetector()) {
            initTfod();
        } else {
            telemetry.addData("Sorry!", "This device is not compatible with TFOD");
        }
    }

    public int getMineralPosition() {
        if (opModeIsActive()) {
            /** Activate Tensor Flow Object Detection. */
            if (tfod != null) {
                tfod.activate();
            }
            runtime.reset();
            while (mineralPosition == 0 && opModeIsActive() && runtime.seconds() <= 5.0) {
                if (tfod != null) {
                    // getUpdatedRecognitions() will return null if no new information is available since
                    // the last time that call was made.
                    List<Recognition> updatedRecognitions = tfod.getUpdatedRecognitions();
                    if (updatedRecognitions != null) {
                        telemetry.addData("# Object Detected", updatedRecognitions.size());
                        if (updatedRecognitions.size() == 3) {
                            int goldMineralX = -1;
                            int silverMineral1X = -1;
                            int silverMineral2X = -1;
                            for (Recognition recognition : updatedRecognitions) {
                                if (recognition.getLabel().equals(LABEL_GOLD_MINERAL)) {
                                    goldMineralX = (int) recognition.getLeft();
                                } else if (silverMineral1X == -1 && updatedRecognitions.size() == 2) {
                                    silverMineral1X = (int) recognition.getLeft();
                                } else if (updatedRecognitions.size() == 3) {
                                    silverMineral2X = (int) recognition.getLeft();
                                } else if (updatedRecognitions.size() == 2) {
                                    goldMineralX = -1;
                                    silverMineral1X = -1;
                                    silverMineral2X = -1;
                                    for (Recognition recognition2 : updatedRecognitions) {
                                        if (recognition.getLabel().equals(LABEL_GOLD_MINERAL)) {
                                            goldMineralX = (int) recognition2.getLeft();
                                        } else if (silverMineral1X == -1) {
                                            silverMineral1X = (int) recognition2.getLeft();
                                        } else {
                                            silverMineral2X = (int) recognition2.getLeft();
                                        }
                                    }
                                }
                                if((goldMineralX < silverMineral1X && goldMineralX < silverMineral2X))
                                {
                                    telemetry.addData("Gold Mineral Position","Right");
                                    telemetry.update();
                                    mineralPosition = 1;
                                }
                                else if(goldMineralX < silverMineral1X || goldMineralX < silverMineral2X)
                                {
                                    telemetry.addData("Gold Mineral Position","Left");
                                    telemetry.update();
                                    mineralPosition = 3;
                                }
                                else if(goldMineralX > silverMineral1X || goldMineralX > silverMineral2X)
                                {
                                    telemetry.addData("Gold Mineral Position","Center");
                                    telemetry.update();
                                    mineralPosition = 2;
                                }

                            }
                            telemetry.update();
                        }
                    }
                }
            }

            if (tfod != null) {
                tfod.shutdown();
            }
        }
        return mineralPosition;
    }

    private void initVuforia() {
        /*
         * Configure Vuforia by creating a Parameter object, and passing it to the Vuforia engine.
         */
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters();

        parameters.vuforiaLicenseKey = VUFORIA_KEY;
        parameters.cameraDirection = CameraDirection.FRONT;

        //  Instantiate the Vuforia engine
        vuforia = ClassFactory.getInstance().createVuforia(parameters);

        // Loading trackables is not necessary for the Tensor Flow Object Detection engine.
    }

    private void initTfod() {
        int tfodMonitorViewId = hardwareMap.appContext.getResources().getIdentifier(
                "tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        TFObjectDetector.Parameters tfodParameters = new TFObjectDetector.Parameters(tfodMonitorViewId);
        tfod = ClassFactory.getInstance().createTFObjectDetector(tfodParameters, vuforia);
        tfod.loadModelFromAsset(TFOD_MODEL_ASSET, LABEL_GOLD_MINERAL, LABEL_SILVER_MINERAL);
        tfodParameters.minimumConfidence = 0.10;

    }



    public void runCenterTurnClockwise(double degrees, double power){
        centerTurnClockwise( degrees, power);

        while (opModeIsActive() && leftMotor.isBusy()) {
            printData();
            idle();

            //      if(!(liftMotor.isBusy())){
            //         stopLift();
            //     }


        }
        initMotorsForward();
        stopMotors();
    }

    public void runDriveTurnClockwise2(double turnRadius,double degrees, double power){

        driveTurnClockwise2( turnRadius, degrees, power);

        while (opModeIsActive() && leftMotor.isBusy()) {
            printData();
            idle();

      //      if(!(liftMotor.isBusy())){
       //         stopLift();
       //     }


        }
        initMotorsForward();
        stopMotors();

    }

    public void runDriveForward(double distanceMilimeters, double power){

        driveForward(distanceMilimeters, power);
        while (opModeIsActive() && leftMotor.isBusy()) {
            printData();
            idle();
            if(!(liftMotor.isBusy())){
                stopLift();
            }

        }

        stopMotors();
        initMotorsForward();
    }

    public void runliftExtendWhileLoop(){

        liftExtend();
        while(opModeIsActive() && liftMotor.isBusy()) {
            printData();
            idle();


        }
        stopLift();

    }

    public void runHookOpen(){
        initHook();
        hookOpen();
        int i = 0;
        printString("OPENING HOOK...");
        while(opModeIsActive()&&i<millisecondsHookOpen){
            i=i+10;
            sleep(10);
        }
        hookStop();
    }

    public void runHookClose(){
        initHook();
        hookClose();
        printString("CLOSING HOOK...");
        int i = 0;
        while(opModeIsActive()&&i<millisecondsHookClose){
            printString("CLOSING HOOK...");
            i=i+10;
            sleep(10);
        }
        hookStop();
    }

    public void runMarkerDrop(){
        initMarker();
        markerDrop();
        int i = 0;
        while(opModeIsActive()&&i<millisecondsHookClose){
            printString("OPENING MARKER...");
            i=i+10;
            sleep(10);
        }
        markerStop();
    }



    public void finishLiftContract(){


        while(opModeIsActive() && liftMotor.isBusy()) {
            printData();
            idle();
        }
        stopLift();

    }




    public double getGyro(){
       return gyro.getAngularOrientation().firstAngle;
    }

    public void initGyro(){

        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();

        parameters.angleUnit = BNO055IMU.AngleUnit.DEGREES;
        parameters.accelUnit = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        gyro.initialize(parameters);

        telemetry.addData("Gyro : ", "Initialized");

    }

    public void printData(){
        int leftTicks = leftMotor.getCurrentPosition();
        int rightTicks = rightMotor.getCurrentPosition();
        int leftTicks2 = leftMotor2.getCurrentPosition();
        int rightTicks2 = rightMotor2.getCurrentPosition();
        int liftTicks = liftMotor.getCurrentPosition();

        String output = "i = " + hookTimeLeft +" LIFT: " + liftTicks + liftMotor.isBusy() + "    Top Left: " + leftTicks + leftMotor.isBusy() + "   Top Right: " + rightTicks + rightMotor.isBusy() + "   Below Left: " + leftTicks2 + leftMotor2.isBusy() + "   Below Right: " + rightTicks2 + rightMotor2.isBusy() ;
        printString(output);
    }

    public void printFinished(){
        int leftTicks = leftMotor.getCurrentPosition();
        int rightTicks = rightMotor.getCurrentPosition();
        int leftTicks2 = leftMotor2.getCurrentPosition();
        int rightTicks2 = rightMotor2.getCurrentPosition();


        String output = "FINISHED - Top Left: " + leftTicks + leftMotor.isBusy() + "   Top Right: " + rightTicks + rightMotor.isBusy() + "   Below Left: " + leftTicks2 + leftMotor2.isBusy() + "   Below Right: " + rightTicks2 + rightMotor2.isBusy() ;
        printString(output);
    }

    public void printString(String output){
        telemetry.addData("Mode", output);
        telemetry.update();
    }

    public void stopAndResetEncoders(){
        leftMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        leftMotor2.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightMotor2.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
    }

    public int mmToPings(double distanceMM){
        double wheelDiameterMM = wheelDiameter*25.4;
        double wheelCircumMM = wheelDiameterMM*3.1415;
        double distancePerMotorRevolutionMM = wheelCircumMM/gearRatio;
        double mmPerPing = distancePerMotorRevolutionMM/motorPPR;
        double pingsPerMM = 1/mmPerPing;
        double numberOfPings =   distanceMM*pingsPerMM;

        return (int)(numberOfPings);
    }



    public void runPath1(){

        runliftExtendWhileLoop();

        runHookOpen();

        printString("TURN #1");

        sleep(0);

        runDriveTurnClockwise2(25*25.4, 80, .35);

        printString("CENTER TURN 105 DEGREES");

        sleep(00);

        runCenterTurnClockwise(-105,0.35);

        printString("FORWARD 20 IN");

        sleep(00);

        runDriveForward(20*26, .40);

        printString("CENTER TURN 75 DEGREES");

        sleep(00);

        runCenterTurnClockwise(75,0.35);

        printString("MARKER");

        sleep(00);

        runMarkerDrop();

        printString("CENTER TURN 65 DEGREES");

        sleep(00);

        runCenterTurnClockwise(65,0.35);

        printString("FORWARD 64 IN");

        sleep(00);

        runDriveForward(64*26, .50);
    }

    public void runPath2(){

        runliftExtendWhileLoop();

        runHookOpen();

        //liftContract();

        runDriveForward(38*25.4, .35);

        // runHookOpen();

        runDriveTurnClockwise2(12*25.4, 45, .35);

        runMarkerDrop();

        runDriveTurnClockwise2(12*25.4, 90, .35);

        runDriveForward(55.5*26, .50);

    }

    public void runPath3() {

        int milliSecPuase = 0;

        runliftExtendWhileLoop();

        runHookOpen();

        printString("FORWARD 14 IN");

        sleep(milliSecPuase);

        runDriveForward(14*26, .40);

        printString("TURN -90 DEG");

        sleep(milliSecPuase);

        runCenterTurnClockwise(-90,0.35);

        printString("REVERSE 4.5 IN");

        sleep(milliSecPuase);

        runDriveForward(-4.5*26, .40);

        printString("2.83 FOOT TURN 90 DEG");

        sleep(milliSecPuase);

        runDriveTurnClockwise2((((2.83)/2)*12)*25.4, 90, .35);

        printString("30 IN, 53 degrees");

        sleep(milliSecPuase);

        runDriveTurnClockwise2((30)*25.4, 53, .35);

        printString("REVERSE 4 IN");

        sleep(milliSecPuase);

        runDriveForward(-4*26, .40);

        printString("TURN 90 DEG");

        sleep(milliSecPuase);

        runCenterTurnClockwise(90,0.35);

        printString("DROP MARKER");

        sleep(milliSecPuase);

        runMarkerDrop();

        printString("TURN -25 DEG");

        sleep(milliSecPuase);

        runCenterTurnClockwise(-25,0.35);

        printString("FORWARD 84 IN");

        sleep(milliSecPuase);

        runDriveForward(84*26, .50);

    }


    public void runPath1_2(){
        int milliSecPuase = 0;

        runliftExtendWhileLoop();

        runHookOpen();

        printString("FORWARD 14 IN");

        sleep(milliSecPuase);

        runDriveForward(14*26, .40);

        printString("TURN -90 DEG");

        sleep(milliSecPuase);

        runCenterTurnClockwise(90,0.35);

        printString("FORWARD 2.0 IN");

        sleep(milliSecPuase);

        runDriveForward(-2.0*26, .40);

        printString("2.83 FOOT TURN -90 DEG");

        sleep(milliSecPuase);

        runDriveTurnClockwise2(-(((2.83)/2)*12)*25.4, 90, -0.35);

        printString("30 IN, 53 degrees");

        sleep(milliSecPuase);

        runDriveTurnClockwise2(-(30)*25.4, 53, -0.35);



        printString("TURN 90 DEG");

        sleep(milliSecPuase);

        runCenterTurnClockwise(90,0.35);

        printString("DROP MARKER");

        sleep(milliSecPuase);

        runMarkerDrop();

        printString("TURN 70 DEG");

        sleep(milliSecPuase);

        runCenterTurnClockwise(70,0.35);

        printString("FORWARD 80 IN");

        sleep(milliSecPuase);

        runDriveForward(80*26, .50);
    }




    public void runSudo(){

       int pathNumber = getMineralPosition();



       switch(pathNumber){
           case 0: runPath2();
           case 1: runPath1_2();
           case 2: runPath2();
           case 3: runPath3();
           break;
       }

       stop();

    }

}
