package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.robotcontroller.external.samples.HardwarePushbot;

@TeleOp(name="TeleOp", group="Pushbot")

public class ArcadeDrive extends LinearOpMode {

    /* Declare OpMode members. */
    HardwarePushbot robot           = new HardwarePushbot();   // Use a Pushbot's hardware
    // could also use HardwarePushbotMatrix class.
    @Override
    public void runOpMode() {
        double left;
        double right;
        double drive;
        double turn;
        double max;

        /* Initialize the hardware variables.
         * The init() method of the hardware class does all the work here
         */
        robot.init(hardwareMap);

        // Send telemetry message to signify robot waiting;
        telemetry.addData("Say", "Hello Driver");    //
        telemetry.update();

        // Wait for the game to start (driver presses PLAY)
        waitForStart();

        // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {

            // Run wheels in POV mode (note: The joystick goes negative when pushed forwards, so negate it)
            // In this mode the Left stick moves the robot fwd and back, the Right stick turns left and right.
            // This way it's also easy to just drive straight, or just turn.
          //  drive = gamepad1.left_stick_y;
            //turn  =  -gamepad1.right_stick_x;
            double theRightDrive;
            double theLeftDrive;

            double   theLeftYJs;
            double theRightXJs;
            double theLeftXJs;
            double theRightYJs;

            theLeftYJs = -1;
            theRightXJs  =  -1;
            theLeftXJs=-1;
            theRightYJs=-1;
            theRightXJs= -gamepad1.right_stick_x;
            theLeftYJs=gamepad1.left_stick_y;
            theLeftXJs=gamepad1.left_stick_x;
            theRightYJs=gamepad1.right_stick_y;



            boolean rightBumperButtonBool = gamepad1.right_bumper;
boolean leftBumperButtonBool = gamepad1.left_bumper;
double leftTriggerDouble = gamepad1.left_trigger;


if(leftBumperButtonBool){



    if (rightBumperButtonBool) {

        theRightDrive = -(theRightXJs * .5);
        theLeftDrive = (theRightXJs * .5);


        double maxRightAdditionVal = 0;
        double maxRightSubtractionVal = 0;

        double maxLeftAdditionVal = 0;
        double maxLeftSubtractionVal = 0;

        double maxAdditionVal = 0;
        double maxSubtractionVal = 0;


        maxRightAdditionVal = 1 - theRightDrive;
        maxRightSubtractionVal = -1 - theRightDrive;

        maxLeftAdditionVal = 1 - theLeftDrive;
        maxLeftSubtractionVal = -1 - theLeftDrive;


        if (maxLeftAdditionVal > maxRightAdditionVal) {
            maxAdditionVal = maxRightAdditionVal;
        }
        if (maxLeftAdditionVal <= maxRightAdditionVal) {
            maxAdditionVal = maxLeftAdditionVal;
        }

        if (maxLeftSubtractionVal < maxRightSubtractionVal) {
            maxSubtractionVal = maxRightSubtractionVal;
        }
        if (maxLeftSubtractionVal >= maxRightSubtractionVal) {
            maxSubtractionVal = maxLeftSubtractionVal;
        }


        if (maxSubtractionVal > theLeftYJs) {
            theRightDrive = theRightDrive + maxSubtractionVal;
            theLeftDrive = theLeftDrive + maxSubtractionVal;
        } else if (maxAdditionVal < theLeftYJs) {
            theRightDrive = theRightDrive + maxAdditionVal;
            theLeftDrive = theLeftDrive + maxAdditionVal;
        } else if (1 == 1) {
            theRightDrive = theRightDrive + theLeftYJs;
            theLeftDrive = theLeftDrive + theLeftYJs;

        }
    } else {
        double reductionValue = 1 - leftTriggerDouble * .7;
        theRightDrive = theRightYJs * reductionValue;
        theLeftDrive = theLeftYJs * reductionValue;
    }





    robot.leftMotor.setPower(-theRightDrive);
    robot.leftMotor2.setPower(-theRightDrive);
    robot.rightMotor.setPower(-theLeftDrive);
    robot.rightMotor2.setPower(-theLeftDrive);




}
else {


    if (rightBumperButtonBool) {

        theRightDrive = -(theRightXJs * .5);
        theLeftDrive = (theRightXJs * .5);


        double maxRightAdditionVal = 0;
        double maxRightSubtractionVal = 0;

        double maxLeftAdditionVal = 0;
        double maxLeftSubtractionVal = 0;

        double maxAdditionVal = 0;
        double maxSubtractionVal = 0;


        maxRightAdditionVal = 1 - theRightDrive;
        maxRightSubtractionVal = -1 - theRightDrive;

        maxLeftAdditionVal = 1 - theLeftDrive;
        maxLeftSubtractionVal = -1 - theLeftDrive;


        if (maxLeftAdditionVal > maxRightAdditionVal) {
            maxAdditionVal = maxRightAdditionVal;
        }
        if (maxLeftAdditionVal <= maxRightAdditionVal) {
            maxAdditionVal = maxLeftAdditionVal;
        }

        if (maxLeftSubtractionVal < maxRightSubtractionVal) {
            maxSubtractionVal = maxRightSubtractionVal;
        }
        if (maxLeftSubtractionVal >= maxRightSubtractionVal) {
            maxSubtractionVal = maxLeftSubtractionVal;
        }


        if (maxSubtractionVal > theLeftYJs) {
            theRightDrive = theRightDrive + maxSubtractionVal;
            theLeftDrive = theLeftDrive + maxSubtractionVal;
        } else if (maxAdditionVal < theLeftYJs) {
            theRightDrive = theRightDrive + maxAdditionVal;
            theLeftDrive = theLeftDrive + maxAdditionVal;
        } else if (1 == 1) {
            theRightDrive = theRightDrive + theLeftYJs;
            theLeftDrive = theLeftDrive + theLeftYJs;

        }
    } else {
        double reductionValue = 1 - leftTriggerDouble * .7;
        theRightDrive = theRightYJs * reductionValue;
        theLeftDrive = theLeftYJs * reductionValue;
    }



    robot.leftMotor.setPower(theLeftDrive);
    robot.leftMotor.setPower(theLeftDrive);
    robot.rightMotor.setPower(theRightDrive);
    robot.rightMotor2.setPower(theRightDrive);
}


            //START OF ARCADE




            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!         CALL THE VALUE TO THE MOTORS
            //FINISH OF ARCADE








            //Lift stuff

            //Power the lift motors.
            if (gamepad2.y == true)
                robot.liftMotor.setPower(-1);
            else
            if (gamepad2.a == true)
                robot.liftMotor.setPower(1);
            //Make lift stop
            if (gamepad2.y == false && gamepad2.a == false)
                robot.liftMotor.setPower(0);
            if (gamepad2.y == true && gamepad2.a == true)
                robot.liftMotor.setPower(0);
            //make the hook move
            if(gamepad2.left_bumper) {
                robot.hook.setDirection(Servo.Direction.FORWARD);
                robot.hook.setPosition(1);
            }
            else {
                if (gamepad2.right_bumper) {
                    robot.hook.setPosition(0);
                }
            }
            if(!gamepad2.right_bumper && !gamepad2.left_bumper)
            {
                robot.hook.setPosition(.5);
            }


            // Send telemetry message to signify robot running;
            telemetry.addData("left",  "%.2f", theLeftDrive);
            telemetry.addData("right", "%.2f", theRightDrive);
            telemetry.update();

            // Pace this loop so jaw action is reasonable speed.
            sleep(50);
        }
    }
}
