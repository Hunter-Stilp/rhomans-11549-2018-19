package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;
import org.firstinspires.ftc.robotcontroller.external.samples.HardwarePushbot;
import com.qualcomm.robotcore.hardware.DcMotor;

@TeleOp(name="EncoderTester", group="Pushbot")

public class EncoderTester extends LinearOpMode {

    /* Declare OpMode members. */
    HardwarePushbot robot = new HardwarePushbot();   // Use a Pushbot's hardware
    // could also use HardwarePushbotMatrix class.

    //@Disable

    @Override

    public void runOpMode() {




        /* Initialize the hardware variables.
         * The init() method of the hardware class does all the work here
         */
        robot.init(hardwareMap);

        // Send telemetry message to signify robot waiting;
     //   telemetry.addData("Say", "Hello Driver");    //
        telemetry.update();






        DcMotor leftDrive;

        leftDrive = hardwareMap.dcMotor.get("left_drive");

        leftDrive.setDirection(DcMotor.Direction.REVERSE);

        // Wait for the game to start (driver presses PLAY)
        waitForStart();

        // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {




            int position = leftDrive.getCurrentPosition();



while(leftDrive.getCurrentPosition()<1000){
leftDrive.setPower(-0.75);
    telemetry.addData("Encoder Position", leftDrive.getCurrentPosition());
    telemetry.update();
}
            leftDrive.setPower(0);

            sleep(50);
        }



    }







}